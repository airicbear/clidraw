#!/usr/bin/env python3

draw_pt = '#'

horz_line = lambda x: ' '.join([draw_pt for _ in range(abs(x))])
horz_line_ends = lambda x: draw_pt + ' '.join([' ' for _ in range(abs(x) - 1)]) + draw_pt
two_vert_lines = lambda x,y: '\n'.join([horz_line_ends(x) for _ in range(abs(y) - 2)])
vert_line = lambda y: '\n'.join([draw_pt for _ in range(abs(y))])
divider = lambda x: '-'.join(['-' for _ in range(x)])